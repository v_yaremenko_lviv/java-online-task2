package com.epam.java.Main;

import java.util.Scanner;

    public class Rumors {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("How much people will be at the Party : ");
            int numberOfPeople = scanner.nextInt();
            if (numberOfPeople < 3) {
                System.out.println("Something is wrong, there should be fewer people");
            }
            else
            {
                Source solve = new Source(numberOfPeople);
                solve.getProbAllSpread();
                scanner.close();
            }
        }
    }


