package com.epam.java.Main;

import java.util.ArrayList;

public class Source {
    private ArrayList<Boolean> allPeople;
    private int G;
    private int spread;
    private double probAll;
    private String probability = "The probability that everyone at the party";
    private String hearTheRumor = " will hear the rumor before it stops propagating is: ";

    public Source(int G) {
        this.G = G;
        allPeople = new ArrayList<Boolean>();
        allPeople.add(true);
        for (int i = 0; i < G-1; i++) {
            allPeople.add(false);
        }
        spread = 1;
        probAll = 1.0;
    }

    public void getProbAllSpread(){
        for (int i = 1; i < G; i++)
        {
            probAll = probAll*(G-i)/(G-1);
        }
        System.out.println( probability + hearTheRumor
                + (double)Math.round(probAll*10000000000000000L)/10000000000000000L
                + " or " + (double)Math.round(probAll*10000000000000000L)/10000000000000000L*100 + "%");
    }
}
